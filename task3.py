import pandas as pd
from dash import Dash, html, dcc

from plotly.express import line


def main():
    dash_app = Dash(__name__)

    df = pd.read_csv('./data/pink_morsel.csv')
    line_chart = line(df, x="date", y="sales", title="Pink Morsel Sales")

    dash_app.layout = html.Div(
        [
            html.H1(
                "Pink Morsel Visualizer",
                id="header",
            ),
            dcc.Graph(
                id="visualization",
                figure=line_chart
            )
        ]
    )

    dash_app.run(debug=True)


# this is only true if the module is executed as the program entrypoint
if __name__ == '__main__':
    main()
