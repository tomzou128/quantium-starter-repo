import pandas as pd


def main():
    df = pd.concat([pd.read_csv(f"./data/daily_sales_data_{i}.csv") for i in range(3)])
    df = df[df['product'].str.contains('pink morsel')]
    df['sales'] = df['quantity'] * [float(s.replace('$', '')) for s in df['price']]
    print(df.head())
    df_2 = df[['sales', 'date', 'region']]
    df_2.to_csv('./data/pink_morsel.csv', index=False)


if __name__ == '__main__':
    main()
